import { Component, Input, OnInit, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { FormioComponent } from 'angular-formio/components/formio/formio.component';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedFormsService } from './advanced-forms.service';
import {ErrorService, TemplatePipe, AppEventService} from '@universis/common';
import {OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';
import * as UNIVERSIS_GRAMMAR from '@universis/grammar';
import { AddDatetimeLocalePreProcessor } from './advanced-forms.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'advanced-form-edit',
  // tslint:disable-next-line: max-line-length
  template: `<formio #form [readOnly]="readOnly" [form]="formConfig" (formLoad)='onLoad($event)'
    [refresh]="refreshForm" [renderOptions]="renderOptions"></formio>`,
  providers: [AdvancedFormsService, TemplatePipe]
})

export class AdvancedFormComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input('data') data: any;
  @Output() dataChange = new EventEmitter<any>();
  @Input('formName') formName: string;
  @ViewChild('form') form: FormioComponent;
  @Output() refreshForm = new EventEmitter<any>();
  @Input('formConfig') formConfig;
  @Output() customEvent = new EventEmitter<any>();
  @Input('readOnly') readOnly: boolean = false;

  @Output() formSubmitted = new EventEmitter<any>();
  private changeSubscription: Subscription;
  private customEventSubscription: Subscription;

  public renderOptions: {
    language: string;
    i18n: any;
  };

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _formService: AdvancedFormsService,
              private _errorService: ErrorService,
              private _router: Router,
              private _template: TemplatePipe,
              private _appEvent: AppEventService) {
  }
  ngAfterViewInit(): void {
    this.form.customEvent = this.customEvent;
    this.customEventSubscription = this.form.customEvent.subscribe((x: any) => {
      if(x.type === 'cancelEvent'){
        if (x.component && x.component.redirect){
          const redirectUrl = this._template.transform(x.component.redirect, this._activatedRoute.snapshot.params)
          this._router.navigate([redirectUrl]);
        } else {
          this._router.navigate(['../'], {relativeTo: this._activatedRoute, replaceUrl: true})
        }
      }
    });

    this.changeSubscription = this.form.change.subscribe((event: any) => {
      if (event && event.data) {
        this.dataChange.emit(event.data);
      }
    });

    this.form.submitExecute = (submission: any) => {
      (async (submissionData: any) => {
        try {
          submissionData.data = JSON.parse(JSON.stringify(submissionData.data), (key, value) => {
            if (value === '' ) {
              value =  null;
            }
            // convert empty object value to null.
            if (value && typeof value === 'object' && Object.keys(value).length === 0) {
              value = null;
            }
            return value;
          });
          return await this._context.getService().execute({
            method: 'POST',
            url: this.formConfig.model,
            headers: {},
            data: submissionData.data
          });
        } catch (error) {
          throw error;
        }
      })(submission).then((result) => {
        this.data = result;
        this.form.onSubmit(submission, true);
        //  Prepare message and notify advance-form-router about form submission
        let toastMessage;
        if ( this.formConfig.submitMessage && this.formConfig.submitMessage[this._translateService.currentLang] ) {
          toastMessage = {
            title: this.formConfig.submitMessage[this._translateService.currentLang].title,
            body: this.formConfig.submitMessage[this._translateService.currentLang].body
          };
        } else {
          toastMessage = {
            title: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitTitle'),
            body: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitMessage')
          };
        }
        const valueEmitted = { toastMessage };
        this.formSubmitted.emit(valueEmitted);
        this._appEvent.change.next({
          model: this.formConfig.model,
          target: result
        });
      }).catch( err => {
        if (err.status) {
          const translatedError = this._translateService.instant(`E${err.status}`);
          this._errorService.showError(translatedError.message, {
            continueLink: '.'
          });
          return this.onError(new Error(translatedError.message))
        }
        this._errorService.showError(err, {
          continueLink: '.'
        });
        this.onError(err);
      });
    };
  }

  onLoad(event) {
    if (event && Object.keys(event).length === 0) {
      return;
    }
    this.refreshForm.emit({
      submission: {
        data: this.data
      }
    });
    if (this.form.formio.wizard) {
      // NOTE: Should be replaced by builder options
      this.form.formio.options.buttonSettings.showNext = false;
      this.form.formio.options.buttonSettings.showPrevious = false;
      this.form.formio.options.buttonSettings.showCancel = false;
      this.form.formio.options.buttonSettings.showSubmit = false;
      this.form.formio.options.breadcrumbSettings.clickable = true;

      // FIXME: These run twice at some point, but it may be fixed
      this.form.formio.on('toNextStep', () => {
        this.form.formio.nextPage();
      });

      this.form.formio.on('toPreviousStep', () => {
        this.form.formio.prevPage();
      });
    }
  }

  async ngOnInit(): Promise<void> {
    try {
      // if formName is provided
      if (this.formName) {
        // load form config
        this.formConfig = await this._formService.loadForm(this.formName);
      }
      if (this.formConfig) {
        // define universis property
        // every service that needs to be available through forms can be added below
        new AddDatetimeLocalePreProcessor(this._translateService).parse(this.formConfig);
        Object.assign(this.formConfig, {
          universis: {
            grammar: UNIVERSIS_GRAMMAR
          }
        });
      }
      const currentLang = this._translateService.currentLang;
      const translation = this._translateService.instant('Forms');
      if (this.formConfig.settings && this.formConfig.settings.i18n) {
        // try to get local translations
        if (Object.prototype.hasOwnProperty.call(this.formConfig.settings.i18n, currentLang)) {
          // assign translations
          Object.assign(translation, this.formConfig.settings.i18n[currentLang]);
        }
      }
      // prepare i18n object
      const i18n = Object.defineProperty({}, currentLang, {
        configurable: true,
        enumerable: true,
        writable: true,
        value: translation
      });
      // and initialize renderOptions
      this.renderOptions = {
        language: currentLang,
        i18n
      };
    } catch (err) {
      this._errorService.navigateToError(err);
    }
  }

  onError(error){
    this.form.onError(error);
  }

  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.customEventSubscription && !this.customEventSubscription.closed){
      this.customEventSubscription.unsubscribe();
    }
  }
}
