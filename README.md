# @universis/forms

Implementation of [angular-formio](https://github.com/formio/angular) for [Universis](https://gitlab.com/universis) client applications.

## Usage

    npm i @universis/forms

Import `AdvancedFormsModule`:

        @NgModule({
        declarations: [
            ...
        ],
        imports: [
            CommonModule,
            BrowserModule,
            BrowserAnimationsModule,
            HttpClientModule,
            TranslateModule.forRoot(),
            SharedModule.forRoot(),
            ...,
            AdvancedFormsModule.forRoot(),
            ...
        ],
        bootstrap: [AppComponent]
        })
        export class AppModule {
          constructor() {
              ...
          }
        }

and start using `AdvancedFormComponent`:

    <advanced-form-edit formName="Courses/edit" [(data)]="model">
    </advanced-form-edit>

or

    <advanced-form-edit [(data)]="model" (customEvent)="onCustomEvent($event)" 
    [formConfig]="formConfiguration" ></advanced-form-edit>

### Development

Import `@universis/forms` as submodule in any angular cli project by replacing `newProjectRoot` as already configured in your `angular.json`

    git submodule add https://gitlab.com/universis/forms.git <newProjectRoot>/forms

e.g.

    git submodule add https://gitlab.com/universis/forms.git projects/forms

Add the following entries to `tsconfig.app.json#compilerOptions.paths`:

    {
        "compilerOptions": {
            "paths": {
                "@universis/forms": [
                    "<newProjectRoot>/forms/src/public_api"
                ]
                ...
            }
        }
    }  

If you want to include `@universis/forms` as an angular cli project, include the following section in `angular.json` under `projects`:

    "projects": {
      "forms": {
        "root": "<newProjectRoot>/forms",
        "sourceRoot": "<newProjectRoot>/forms/src",
        "projectType": "library",
        "prefix": "lib",
        "architect": {
          "build": {
            "builder": "@angular-devkit/build-ng-packagr:build",
            "options": {
              "tsConfig": "<newProjectRoot>/forms/tsconfig.lib.json",
              "project": "<newProjectRoot>/forms/ng-package.json"
            }
          },
          "test": {
            "builder": "@angular-devkit/build-angular:karma",
            "options": {
              "main": "<newProjectRoot>/forms/src/test.ts",
              "polyfills": "<newProjectRoot>/forms/src/polyfills.ts",
              "tsConfig": "<newProjectRoot>/forms/tsconfig.spec.json",
              "karmaConfig": "<newProjectRoot>/forms/karma.conf.js",
              "watch": false,
              "codeCoverage": true,
              "scripts": []
            }
          },
          "lint": {
            "builder": "@angular-devkit/build-angular:tslint",
            "options": {
              "tsConfig": [
                "<newProjectRoot>/forms/tsconfig.lib.json",
                "<newProjectRoot>/forms/tsconfig.spec.json"
              ],
              "exclude": [
                "**/node_modules/**"
              ]
            }
          }
        }
      },
      ...
    }
